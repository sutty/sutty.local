# Especify the domain here or as an argument
domain ?= sutty.local
serial ?= $(shell date +%s)

# Never remove private keys!
.PRECIOUS: %.key

.DEFAULT_GOAL := all

ca_key ?= ca/key.key
ca_crt ?= ca/crt.crt
ca_tpl ?= templates/ca.tpl

domain_key ?= domain/$(domain).key
domain_csr ?= domain/$(domain).csr
domain_crt ?= domain/$(domain).crt
domain_tpl ?= templates/domain.tpl

# Make variables available to templates
export

# Detect distribution
include /etc/os-release

# Render templates
templates/%.tpl: templates/%.tpl.in
	envsubst < $< > $@

# Generate private keys
%.key:
	mkdir -p $(dir $@)
	certtool --generate-privkey --ask-pass --sec-param medium --outfile $@
	chmod 600 $@

# Generates a self-signed key for the CA
$(ca_crt): $(ca_key)
	certtool --generate-self-signed \
	         --load-privkey $< \
	         --template $(ca_tpl) \
	         --outfile $@

# Generates a certificate signing request
domain/%.csr: $(domain_tpl)
	certtool --generate-request --load-privkey $(domain_key) --outfile $@ --template $<

# Generates a site certificate
domain/%.crt:
	certtool --generate-certificate --load-request $(domain_csr) \
	         --load-ca-certificate $(ca_crt) \
	         --load-ca-privkey $(ca_key) \
	         --template $(domain_tpl) \
	         --outfile $@

ca: $(ca_key) $(ca_crt) ## Generate the CA keypair
install-ca: $(ca_crt)   ## Install the CA on system
ifeq ($(ID_LIKE),debian)
	sudo install -Dm 644 $< /usr/share/ca-certificates/extra/sutty.crt
	sudo dpkg-reconfigure ca-certificates
	sudo update-ca-certificates
else
	sudo trust anchor $<
endif
all: ca install-ca      ## Generate and install
domain: $(domain_key) $(domain_csr) $(domain_crt) ## Issue a site certificate
	rm -f templates/domain.tpl
coopcloud: ca domain
	abra app secret insert traefik.$(domain) ssl_cert v1 "`cat $(domain_crt)`"
	abra app secret insert traefik.$(domain) ssl_key  v1 "`cat $(domain_key)`"
