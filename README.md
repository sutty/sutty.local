# Deprecated, see [haini.sh](https://0xacab.org/sutty/haini.sh)

# Sutty Certificate Authority

The Sutty CA generates a certificate we can trust in our development
environment and use to work with all the nic

## Usage

Install `certtools`, it comes with GnuTLS (the package is called
`gnutls-tools` or something like in _certain_ distributions).

Generate and install the CA:

```bash
make all
```

Generate a certificate for each site.  We use domains in the `.local`
domain, but it could be anything:

```bash
make domain domain=sutty.local
```

Then install the generated private key and certificate into your
project, for instance:

```bash
# For the platform
cp domain/sutty.local.* ../sutty/config/
cd ../sutty
rails s -b "ssl://localhost:3000?key=sutty/config/sutty.local.key&cert=sutty/config/sutty.local.crt"

# For static websites
cp domain/sutty.local.* ../sutty.nl/
nghttpd -d _site 8080 sutty.local.{key,crt}
```

### DNS resolver

You'll probably need to add the fake domains in your `/etc/hosts`, or
even run your local DNS resolver that redirects all .local to localhost!

```bash
# This method doesn't required anything extra but it doesn't support
# wildcard certificates so you need to run it once per domain
echo "127.0.0.1 sutty.local" | sudo tee -a /etc/hosts
```

This method requires a few extra steps but it works automatically
afterwards:

* Install `dnsmasq`

* Add the configuration option `address=/local/127.0.0.1` to
  `/etc/dnsmasq.conf` or run the program with the
  `--address=/local/127.0.0.1` flag.

* Add a new DNS server on top of your regular DNS resolvers in your
  network manager or directly into `/etc/resolv.conf`.

## How can this work

A certificate authority is a certificate that is trusted by the local
system.  Any certificate signed (trusted) by this certificate is
automatically trusted.

Handle with care!  You can even issue certificates for sites in the
Internet! ;)

For more info, please refer to the
[documentation](https://help.ubuntu.com/community/GnuTLS).
