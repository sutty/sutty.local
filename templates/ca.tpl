# X.509 Certificate options
#
# DN options

# The organization of the subject.
organization = "Sutty"

# The organizational unit of the subject.
#unit = "sleeping dept."

# The state of the certificate owner.
state = "Cyberspace"

# The country of the subject. Two letter code.
country = IN

# The common name of the certificate owner.
cn = "Sutty Local CA"

# The serial number of the certificate. Should be incremented each time a new certificate is generated.
serial = 007

# In how many days, counting from today, this certificate will expire.
expiration_days = 3650

# Whether this is a CA certificate or not
ca

# Whether this key will be used to sign other certificates.
cert_signing_key

# Whether this key will be used to sign CRLs.
crl_signing_key
